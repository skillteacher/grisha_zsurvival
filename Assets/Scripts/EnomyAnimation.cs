using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnomyAnimation : MonoBehaviour
{
    public event Message AnimationAttackMoment;

    public void AnimAttack()
    {
        AnimationAttackMoment?.Invoke();
    }
}
public delegate void Message();