using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    [SerializeField] private EnomyAnimation enemyAnimation;
     private Transform target;
    [SerializeField] private float damage = 40f;
    private void Start()
    {
        target = targetsforenomy.Instance.GetTarget();
    }
    private void OnEnable()
    {
        enemyAnimation.AnimationAttackMoment += AttackTarget;
    }
    private void OnDisable()
    {
        enemyAnimation.AnimationAttackMoment -= AttackTarget;
    }
    public void AttackTarget()
    {
        target.GetComponent<PlayerHealth>().TakeDamage(damage);
    }
}
