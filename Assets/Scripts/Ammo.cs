using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ammo : MonoBehaviour
{
    [SerializeField] private int ammoAmount = 10;
    [SerializeField] private Text ammoText;
    private void Start()
    {
        ammoText = GameObject.FindGameObjectWithTag("AmmoText").GetComponent<Text>();
        ammoText.text = ammoAmount.ToString();
    }

    public void AddAmmo(int amount)
    {
        ammoAmount += amount;
        ammoText.text = ammoAmount.ToString();
    }
    public void ReduceAmmo()
    {
        ammoAmount --;
        ammoText.text = ammoAmount.ToString();
    }

    public bool IsNoAmmo()
    {
        return ammoAmount <= 0;
    
    }
}
