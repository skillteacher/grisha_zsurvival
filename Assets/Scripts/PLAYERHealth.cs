using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public event Message PlayerDeath;
    [SerializeField] private float Health = 100f;


    public void TakeDamage(float damageamount)
    {
        Health -= damageamount;

        if (Health <= 0)
        {
            Debug.Log("�� ���� :]");
            PlayerDeath?.Invoke();
        }
    }
  
}

