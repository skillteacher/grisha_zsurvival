using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private float damage = 50f;
    [SerializeField] private float range = 100f;
    [SerializeField] private float delay = 0.2f;
    [SerializeField] private ParticleSystem muzzleFlashEffect;
    [SerializeField] private GameObject SparksEffect;
    [SerializeField] private float sparksLifeTime = 0.1f;
    [SerializeField] private Ammo ammo;

    private void Start()
    {
        
    }

    private bool isReadyToShoot = true;

    private void Update()
    {
        if (Input.GetButtonDown("Fire1") && isReadyToShoot)
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        if (ammo.IsNoAmmo())
        {
            return;
        }

        ammo.ReduceAmmo();
        PlayMuzzleFlash();
        Raycasting();
        StartCoroutine(DelayCountdown(delay));
    }
    private void PlayMuzzleFlash()
    {
        muzzleFlashEffect.Play();
    }
    private void Raycasting()
    {
        RaycastHit hit;
        if (!Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, range)) return;
        SummonSparks(hit.point);
        Health targetHeath = hit.transform.GetComponent<Health>();
        if (targetHeath == null) return;
        targetHeath.TakeDamage(damage);
    }
    private IEnumerator DelayCountdown(float delay)
    {
        isReadyToShoot = false;
        yield return new WaitForSeconds(delay);
        isReadyToShoot = true;
    }

    private void SummonSparks(Vector3 point)

    {
       GameObject sparks = Instantiate(SparksEffect, point, Quaternion.identity);
        Destroy(sparks, sparksLifeTime);
    }
}