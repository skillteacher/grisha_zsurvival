using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private float healthAmount = 100f;

   public void TakeDamage(float damageamount)
    { 
        healthAmount -= damageamount;

        if (healthAmount <= 0)  
        {
            Destroy(gameObject);
        }
    }




}
