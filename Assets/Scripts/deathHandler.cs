using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deathHandler : MonoBehaviour
{
    [SerializeField] private Canvas gameOverCanvas;
    private PlayerHealth playerHealth;
    

    private void Start()
    {
        gameOverCanvas.enabled = false;
        Time.timeScale = 1f;

        

    }

    private void OnEnable()
    {
        playerHealth = targetsforenomy.Instance.GetTarget().GetComponent<PlayerHealth>();
        playerHealth.PlayerDeath += OnPlayerDeath;
    }
    private void OnDisable()
    {
        playerHealth.PlayerDeath -= OnPlayerDeath;
    }

    private void OnPlayerDeath()
    {
        gameOverCanvas.enabled = true;
        Time.timeScale = 0f;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
